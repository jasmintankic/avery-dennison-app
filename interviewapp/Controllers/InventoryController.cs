﻿using interviewapp.Data;
using interviewapp.Models;
using interviewapp.Projections;
using interviewapp.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace interviewapp.Controllers
{
    [Route("api/inventory")]
    [ApiController]
    public class InventoryController : ControllerBase
    {
        private readonly InventoryDataService _inventoryDataService;
        public InventoryController(InventoryDataService inventoryDataService) 
        {
            _inventoryDataService = inventoryDataService;
        }

        [HttpPost("add-item")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult PostInventoryData(InventoryData postData)
        {
            _inventoryDataService.ValidateAndAddItemsToInventory(postData);
            return Ok();
        }

        [HttpPost("create")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult CreateInventory(string inventoryName)
        {
            _inventoryDataService.CreateInventory(inventoryName);
            return Ok();
        }

        [HttpGet("count-by-company")]
        public List<InventoriedItemsCountByCompany> GetItemsCountByCompany()
        {
            return _inventoryDataService.GetItemsCountByCompany();
        }

        [HttpGet("count-by-product-per-day")]
        public List<InventoriedItemsCountBySpecificProductPerDay> GetItemsCountByProductPerDay(string optionalDate)
        {
            return _inventoryDataService.GetItemsCountByProductPerDay(optionalDate);
        }


        [HttpGet("count-by-product-inventory")]
        public List<InventoriedItemsByProductInventory> GetItemsCountByProductForInventory(string optionalInventoryId)
        {
            return _inventoryDataService.GetItemsCountByProductInventory(optionalInventoryId);
        }
    }
}
