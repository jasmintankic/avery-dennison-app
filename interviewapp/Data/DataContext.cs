﻿using interviewapp.Models;
using interviewapp.Projections;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace interviewapp.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Inventory> Inventories { get; set; }
        public DbSet<ProductInventory> ProductInventories { get; set; }

        public DbSet<InventoriedItemsCountByCompany> InventoriedItemsCountByCompanies { get; set; }

        public DbSet<InventoriedItemsCountBySpecificProductPerDay> InventoriedItemsCountBySpecificProducts { get; set; }

        public DbSet<InventoriedItemsByProductInventory> InventoriedItemsByProductInventories { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductInventory>().HasKey(pi => new { pi.Id, pi.ProductId, pi.InventoryId });

            modelBuilder.Entity<InventoriedItemsCountByCompany>().HasNoKey();
            modelBuilder.Entity<InventoriedItemsCountByCompany>().ToTable(nameof(InventoriedItemsCountByCompanies), t => t.ExcludeFromMigrations());

            modelBuilder.Entity<InventoriedItemsCountBySpecificProductPerDay>().HasNoKey();
            modelBuilder.Entity<InventoriedItemsCountBySpecificProductPerDay>().ToTable(nameof(InventoriedItemsCountBySpecificProducts), t => t.ExcludeFromMigrations());

            modelBuilder.Entity<InventoriedItemsByProductInventory>().HasNoKey();
            modelBuilder.Entity<InventoriedItemsByProductInventory>().ToTable(nameof(InventoriedItemsByProductInventories), t => t.ExcludeFromMigrations());
        }
    }
}
