﻿using interviewapp.Data;
using interviewapp.Models;
using interviewapp.Projections;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace interviewapp.Services
{
    public class InventoryDataService
    {

        private readonly DataContext _context = null;
        public InventoryDataService(DataContext context)
        {
            _context = context;
        }

        public void ValidateAndAddItemsToInventory(InventoryData postData) {
            VerifyIfInventoryExist(postData.GetInventoryGuid());

            HashSet<ProductInventory> productsToAdd = GetProductsForInventory(postData);

            PersistInventoryItems(productsToAdd);
        }

        public List<InventoriedItemsCountByCompany> GetItemsCountByCompany() {
            return _context.InventoriedItemsCountByCompanies.FromSqlRaw("select p.\"CompanyName\", count (pi2.\"ProductId\") as ItemsCount " +
                "from \"Products\" p left join \"ProductInventories\" pi2 on p.\"Id\" = pi2.\"ProductId\" " +
                "group by p.\"CompanyName\"").ToList();
        }

        public List<InventoriedItemsCountBySpecificProductPerDay> GetItemsCountByProductPerDay(string date)
        {
            string whereClause = date != null ? $"where pi2.\"DateOfInventory\"::date = '{date}'" : "";

            return _context.InventoriedItemsCountBySpecificProducts.FromSqlRaw(String.Format("select pi2.\"DateOfInventory\", p.\"ProductName\" , count (pi2.\"ProductId\") as ItemsCount " +
                "from \"Products\" p join \"ProductInventories\" pi2 on p.\"Id\" = pi2.\"ProductId\" " +
                " {0} " +
                "group by p.\"ProductName\", pi2.\"DateOfInventory\" " +
                "order by p.\"ProductName\", pi2.\"DateOfInventory\"", whereClause)).ToList();
        }

        public List<InventoriedItemsByProductInventory> GetItemsCountByProductInventory(string inventoryId)
        {
            string whereClause = inventoryId != null ? $"where i.\"Id\" = '{inventoryId}'" : "";

            return _context.InventoriedItemsByProductInventories.FromSqlRaw(String.Format("select p.\"ProductName\", i.\"Name\" as InventoryName , i.\"Id\" as InventoryId, count (pi2.\"ProductId\") as ItemsCount from \"Products\" p " +
                "left join \"ProductInventories\" pi2 on p.\"Id\" = pi2.\"ProductId\" " +
                "join \"Inventories\" i on i.\"Id\" = pi2.\"InventoryId\" " +
                " {0} " +
                "group by p.\"ProductName\", i.\"Name\", i.\"Id\"", whereClause)).ToList();
        }

        private HashSet<ProductInventory> GetProductsForInventory(InventoryData postData)
        {
            HashSet<EpcProductDetails> epcProductDetails = GetEpcDetailsFromHexValue(postData.Items);

            HashSet<ProductInventory> productsToAdd = new();

            foreach (EpcProductDetails productEpcDetails in epcProductDetails)
            {
                Product existingProduct = _context.Products.Where(p => p.ItemRefference == productEpcDetails.ItemRefference && p.CompanyPrefix == productEpcDetails.CompanyPrefix).FirstOrDefault();

                if (existingProduct == null)
                {
                    throw new BadHttpRequestException($"Provided item tag does not exist: {productEpcDetails.HexValue}");
                }

                productsToAdd.Add(ProductInventory.CreateFromInventoryDataAndProductId(postData, existingProduct.Id));
            }

            return productsToAdd;
        }

        private void VerifyIfInventoryExist(Guid inventoryId) {
            Inventory existingInventory = _context.Inventories.Find(inventoryId);

            if (existingInventory == null)
            {
                throw new BadHttpRequestException("Provided inventory does not exist.");
            }
        }

        private void PersistInventoryItems(HashSet<ProductInventory> itemsToAdd) {
            try
            {
                _context.ProductInventories.AddRange(itemsToAdd);
                _context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                throw new BadHttpRequestException("Error occured while saving to database. Provided items are already in Inventory.");
            }
            catch (Exception)
            {
                throw new Exception("Unexpected error occured while saving records to database.");
            }
        }

        public void CreateInventory(string inventoryName)
        {
            try {
                Inventory newInventory = new();
                newInventory.Id = Guid.NewGuid();
                newInventory.Name = inventoryName;

                _context.Inventories.Add(newInventory);
                _context.SaveChanges();
            }
            catch (Exception)
            {
                throw new Exception("Unexpected error occured while saving records to database.");
            }
        }

        public static HashSet<EpcProductDetails> GetEpcDetailsFromHexValue(HashSet<string> hexValues)
        {
            HashSet<EpcProductDetails> epcDetails = new();

            foreach (string value in hexValues)
            {
                if (!IsValidHexadecimalValue(value))
                {
                    throw new BadHttpRequestException($"Provided item tag is not valid: {value}");
                }

                epcDetails.Add(EpcProductDetails.fromHexValue(value));
            }

            return epcDetails;
        }

        public static bool IsValidHexadecimalValue(string test)
        {
            // For C-style hex notation (0xFF) you can use @"\A\b(0[xX])?[0-9a-fA-F]+\b\Z"
            return System.Text.RegularExpressions.Regex.IsMatch(test, @"\A\b[0-9a-fA-F]+\b\Z");
        }
    }
}
