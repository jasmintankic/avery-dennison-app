﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using interviewapp.Data;

namespace interviewapp.Migrations
{
    [DbContext(typeof(DataContext))]
    partial class DataContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 63)
                .HasAnnotation("ProductVersion", "5.0.4")
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            modelBuilder.Entity("interviewapp.Models.Inventory", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(50)
                        .HasColumnType("uuid");

                    b.Property<string>("Name")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.ToTable("Inventories");
                });

            modelBuilder.Entity("interviewapp.Models.Product", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("CompanyName")
                        .HasColumnType("text");

                    b.Property<int>("CompanyPrefix")
                        .HasColumnType("integer");

                    b.Property<int>("ItemRefference")
                        .HasColumnType("integer");

                    b.Property<string>("ProductName")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.ToTable("Products");
                });

            modelBuilder.Entity("interviewapp.Models.ProductInventory", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<long>("ProductId")
                        .HasColumnType("bigint");

                    b.Property<Guid>("InventoryId")
                        .HasColumnType("uuid");

                    b.Property<DateTime>("DateOfInventory")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("Location")
                        .HasColumnType("text");

                    b.HasKey("Id", "ProductId", "InventoryId");

                    b.HasIndex("InventoryId");

                    b.HasIndex("ProductId");

                    b.ToTable("ProductInventories");
                });

            modelBuilder.Entity("interviewapp.Projections.InventoriedItemsByProductInventory", b =>
                {
                    b.Property<Guid>("InventoryId")
                        .HasColumnType("uuid");

                    b.Property<string>("InventoryName")
                        .HasColumnType("text");

                    b.Property<int>("ItemsCount")
                        .HasColumnType("integer");

                    b.Property<string>("ProductName")
                        .HasColumnType("text");

                    b.ToTable("InventoriedItemsByProductInventories", t => t.ExcludeFromMigrations());
                });

            modelBuilder.Entity("interviewapp.Projections.InventoriedItemsCountByCompany", b =>
                {
                    b.Property<string>("CompanyName")
                        .HasColumnType("text");

                    b.Property<int>("ItemsCount")
                        .HasColumnType("integer");

                    b.ToTable("InventoriedItemsCountByCompanies", t => t.ExcludeFromMigrations());
                });

            modelBuilder.Entity("interviewapp.Projections.InventoriedItemsCountBySpecificProductPerDay", b =>
                {
                    b.Property<DateTime>("DateOfInventory")
                        .HasColumnType("timestamp without time zone");

                    b.Property<int>("ItemsCount")
                        .HasColumnType("integer");

                    b.Property<string>("ProductName")
                        .HasColumnType("text");

                    b.ToTable("InventoriedItemsCountBySpecificProducts", t => t.ExcludeFromMigrations());
                });

            modelBuilder.Entity("interviewapp.Models.ProductInventory", b =>
                {
                    b.HasOne("interviewapp.Models.Inventory", "Inventory")
                        .WithMany("ProductInventories")
                        .HasForeignKey("InventoryId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("interviewapp.Models.Product", "Product")
                        .WithMany("ProductInventories")
                        .HasForeignKey("ProductId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Inventory");

                    b.Navigation("Product");
                });

            modelBuilder.Entity("interviewapp.Models.Inventory", b =>
                {
                    b.Navigation("ProductInventories");
                });

            modelBuilder.Entity("interviewapp.Models.Product", b =>
                {
                    b.Navigation("ProductInventories");
                });
#pragma warning restore 612, 618
        }
    }
}
