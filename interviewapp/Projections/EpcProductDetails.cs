﻿using Bytefeld.Epc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace interviewapp.Projections
{
    public class EpcProductDetails
    {
        public int CompanyPrefix { get; set; }
        public int ItemRefference { get; set; }
        public int SerialNumber { get; set; }
        public string HexValue { get; set; }

        public static EpcProductDetails fromHexValue(string hexValue) {
            try
            {
                string uri = EpcTag.FromBinary(hexValue).ToString();
                string[] splittedUri = uri.Split('.');

                EpcProductDetails epcDetails = new EpcProductDetails();
                epcDetails.CompanyPrefix = Int32.Parse(splittedUri[1]);
                epcDetails.ItemRefference = Int32.Parse(splittedUri[2]);
                epcDetails.SerialNumber = Int32.Parse(splittedUri[3]);
                epcDetails.HexValue = hexValue;

                return epcDetails;
            } catch (Exception) {
                throw new Exception("Unexpected error occured while converting hex value to EpcDetails.");
            }
        }
    }
}
