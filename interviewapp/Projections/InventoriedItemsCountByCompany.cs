﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace interviewapp.Projections
{
    public class InventoriedItemsCountByCompany
    {
        public string CompanyName { get; set; }
        public int ItemsCount { get; set; }
    }
}
