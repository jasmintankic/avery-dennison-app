﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace interviewapp.Projections
{
    public class InventoriedItemsCountBySpecificProductPerDay
    {
        public DateTime DateOfInventory { get; set; }
        public string ProductName { get; set; }
        public int ItemsCount { get; set; }

    }
}
