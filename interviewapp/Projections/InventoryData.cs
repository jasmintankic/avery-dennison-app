﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace interviewapp.Projections
{
    public class InventoryData
    {
        [Required]
        [MaxLength(50)]
        public string InventoryId { get; set; }
        [Required]
        public string Location { get; set; }
        [Required]
        public DateTime DateOfInventory { get; set; }
        [Required]
        public HashSet<String> Items { get; set; }

        public Guid GetInventoryGuid()
        {
            try
            {
                return new Guid(InventoryId);
            }
            catch (Exception) {
                throw new ArgumentException("Provided InventoryId is not valid.");
            }
        }
    }
}
