﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace interviewapp.Projections
{
    public class InventoriedItemsByProductInventory
    {
        public string ProductName { get; set; }
        public string InventoryName { get; set; }
        public Guid InventoryId { get; set; }
        public int ItemsCount { get; set; }
    }
}
