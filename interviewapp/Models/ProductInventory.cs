﻿using interviewapp.Projections;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace interviewapp.Models
{
    public class ProductInventory
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public long Id { get; set; }
        public long ProductId { get; set; }
        public Product Product { get; set; }
        public Guid InventoryId { get; set; }
        public Inventory Inventory { get; set; }
        public string Location { get; set; }
        public DateTime DateOfInventory { get; set; }

        public static ProductInventory CreateFromInventoryDataAndProductId(InventoryData postInventoryData, long productId) {
            ProductInventory productInventory = new();
            productInventory.DateOfInventory = postInventoryData.DateOfInventory;
            productInventory.Location = postInventoryData.Location;
            productInventory.InventoryId = postInventoryData.GetInventoryGuid();
            productInventory.ProductId = productId;

            return productInventory;
        }

    }
}
