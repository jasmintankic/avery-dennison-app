﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace interviewapp.Models
{
    public class Product
    {
        public long Id { get; set; }
        public int CompanyPrefix { get; set; }
        public string CompanyName { get; set; }
        public int ItemRefference { get; set; }
        public string ProductName { get; set; }

        [JsonIgnore]
        public ICollection<ProductInventory> ProductInventories { get; set; }
    }
}
