﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using interviewapp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using interviewapp.Projections;
using Microsoft.AspNetCore.Http;

namespace interviewapp.Services.Tests
{
    [TestClass()]
    public class InventoryDataServiceTests
    {
        [TestMethod()]
        public void Verify_epc_product_details_conversion_from_hex()
        {
            HashSet<EpcProductDetails> epcProductDetails = InventoryDataService.GetEpcDetailsFromHexValue(new HashSet<string> { "3098D0A357783C0034E9DF74" });

            EpcProductDetails productDetails = epcProductDetails.First();

            Assert.IsNotNull(epcProductDetails);
            Assert.AreEqual(1, epcProductDetails.Count);
            Assert.AreEqual(213645, productDetails.CompanyPrefix);
            Assert.AreEqual(6152432, productDetails.ItemRefference);
            Assert.AreEqual(887742324, productDetails.SerialNumber);
        }

        [TestMethod()]
        [ExpectedException(typeof(BadHttpRequestException), "Provided item tag is not valid: !@#")]
        public void Verify_epc_product_details_conversion_fails_from_invalid_hex()
        {
            HashSet<EpcProductDetails> epcProductDetails = InventoryDataService.GetEpcDetailsFromHexValue(new HashSet<string> { "!@#" });
        }

        [TestMethod()]
        public void Verify_if_provided_hex_is_valid()
        {
            bool result = InventoryDataService.IsValidHexadecimalValue("3098D0A357783C0034E9DF74");
            Assert.IsTrue(result);
        }

        [TestMethod()]
        public void Verify_if_provided_hex_is_invalid()
        {
            bool result = InventoryDataService.IsValidHexadecimalValue("!@#123123sdasdasd");
            Assert.IsFalse(result);
        }
    }
}